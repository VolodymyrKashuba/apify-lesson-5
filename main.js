const Apify = require('apify');
const ApifyClient = require('apify-client');

const apifyClient = new ApifyClient({ token: 'qo56Ct73wL6j7n3dC2xvGJif9' });

Apify.main(async () => {
    const input = await Apify.getInput();
    await apifyClient.task('LfoCzUYDjyBp3tvO9').call({}, {
        memory: input.memory,
    });
    const datasetClient = apifyClient.dataset('fvKNckBmnySuDSosz');
    const items = await datasetClient.downloadItems('csv', {
        fields: input.fields,
        limit: input.maxItems,
    });
    await Apify.setValue('OUTPUT.csv', items, {
        contentType: 'text/csv',
    });
});

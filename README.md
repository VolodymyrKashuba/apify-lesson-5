# Apify lesson V



### Quiz answers

<!-- toc start -->
- What is the relationship between actor and task?
  - Task is the configuration of an actor to perform a specific job
- What are the differences between default (unnamed) and named storage? Which one would you choose for everyday usage?
  - Unnamed datasets expire after 7 days unless otherwise specified
  - Named datasets are retained indefinitely
  - For everyday usage i would choose Unnamed datasets
- What is the relationship between the Apify API and the Apify client? Are there any significant differences?
  - The Apify API  provides programmatic access to the Apify platform and is organized around RESTful HTTP endpoints
  - Apify client is the library to access Apify API from your JavaScript applications
  - There is no difference in usage  between Apify API and Apify client cause Apify client functions correspond to the Apify API endpoints and have the same parameters
- What is data retention and how does it work for all types of storage (default and named)?
  - Data retention is continued storage of data
  - Unnamed storages expire after 7 days unless otherwise specified.
  - Named storages are retained indefinitely.
- How do you pass input when running an actor or task via the API?
  - The POST payload including its Content-Type header is passed as INPUT
 <!-- toc end -->
